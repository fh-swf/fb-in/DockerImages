# Docker Images
Eine Sammlung nutzbarer Docker Images für maschinelles Lernen.
## Mögliche Images
Hier findet sich eine Auflistung möglicher Images.
### Basissystem maschinelles Lernen
Basissysteme, welche für die meisten Anwendungen von maschinellen Lernen genügen. Sie beinhalten folgende Features:
- Ubuntu 18.04
- Python 3.6
- Git
- Numpy 1.16.2
- Tensorflow 1.14.0 (inklusive Tensorboard)
- Scikit-Learn 0.21.3
- Keras 2.2.4
- Matplotlib 3.0.3
- PyTorch 1.2.0

Die GPU-Version beinhaltet zusätzlich Nvidia CUDA in der Version 10.1 und den passenden Nvidia 418.67 Treiber. Einer Installation dieser auf dem Basissystem ist ebenfalls nötig.

Zum Builden der Images sind folgende Befehle zu nutzen:
- CPU Version: docker build --tag machine-learning-cpu https://gitlab.com/fh-swf/fb-in/DockerImages.git#:machine-learning-cpu
- GPU Version: docker build --tag machine-learning-gpu https://gitlab.com/fh-swf/fb-in/DockerImages.git#:machine-learning-gpu

Ein neuer Container kann dann folgendermaßen ausgeführt werden:
- CPU Version: docker run -it machine-learning-cpu
- GPU Version: docker run -it --gpus {all|anzahl} machine-learning-gpu

Die Funktionsfähigkeit der Container wird dann über folgende Befehle getestet:
- CPU Version: python /tmp/testcpu.py
- GPU Version: python /tmp/testgpu.py
